danielnov90
tomn96



=============================
=      File description     =
=============================


package oop.ex6.main:

Sjavac.java - This class is a sjava code validation check

SjavacException.java - This Exception is used as the most general sjava problem that our sjavac validation
                       process has discovered


package oop.ex6.parser:

LineEditor.java - The class is responsible of splitting and editing (slightly) lines.

Parser.java - The class is responsible of parsing the given sjava file. It contains a single static parse
              method which implements the parsing process.

RegExpHandler.java - The class handles with a match between a regular expression, and given string. The
                     class contains 2 static methods.

StructuralException.java - The exception is thrown whenever a structural problem happened.

SuffixException.java - An exception class which extend ScopeException. The exception is thrown whenever
                       there is a problem with the suffix of on of file's line.


package oop.ex6.scopes:

ConditionalException.java - An exception class which extends ScopeException. The exception is thrown when
                            there is a general error in the conditional (if/while) scope.

ConditionalScope.java - This class represents a conditional scope in an sjava code, i.e. if or while
                        scopes.

InvalidParameterException.java - An exception class which extend ScopeException. The exception is thrown
                                 when one of the parameters of the method call, or the content of the
                                 if/while statement is not legal.

MainScope.java - This class represents the main scope in an sjava code.

MethodDuplicateException.java - An exception class which extend MethodException. The exception is thrown
                                when the program identifies 2 methods with the same name.

MethodException.java - An exception class which extend ScopeException. The exception is thrown when there
                       is a general problem which is related to the method scope.

MethodScope.java - This class represents a method scope in an sjava code.

NotReadableStatementException.java - An exception class which extend ScopeException. The exception is
                                    thrown when the statement is not readable in the structural aspect.

PredicateFactory.java - This class is a factory for predicates that does a low level validation of
                        statements.

Scope.java - This class is an abstract class represents a scope in an sjava code.

ScopeException.java - The exception is thrown when there is a general Scope error.

ScopeFactory.java - The ScopeFactory class is responsible of getting a list of code lines and separate it
                    into scopes which are instances of Scope class.

UnhandableLineException.java - The exception is thrown when the program doesn't know how to handle or
                               'classify' a certain line.

UnknownMethodCallException.java - The exception is thrown when there is a method call to a non exciting
                                  method.

VariableDuplicateException.java - The exception is thrown whenever the program encounters with a declared
                                  variable which has an already taken variable name.


package oop.ex6.variables:

AssignmentTypeException.java - The exception is thrown whenever the type of the assignment value/variable
                               does not fits the type of the declared variable.

BooleanVariable.java - A variable which represents a boolean variable.

ChangeFinalVariableException.java - The exception is thrown whenever there is an attempt to change final
                                    variable's assignment.

CharVariable.java - A variable which represents a character variable.

DoubleVariable.java - A variable which represents a double variable.

IntVariable.java - A variable which represents an int variable.

InvalidNameException.java - The exception is thrown if the name of the declared variable is not legal.

NonInitializedVariableAssignmentException.java - The exception is thrown if there is an attempt to assign
                                                 a value/variable to a variable which has never been
                                                 declared.

StringVariable.java - A variable which represents a string type variable.

Variable.java - An abstract class which represents a single variable.

VariableException.java - The exception is thrown when there is a problem which is related to a variable.

VariableHandler.java - This class handles with variable related issues. (see Design)


package oop.ex6:

Tuple.java - A generic class which represent a tuple of 2 types.



=============================
=          Design           =
=============================


Packages division -
We have decided to divide the code into different packages using program’s logic and program’s   flow. The
program holds 4 Packages:
Main - which holds program’s main class which operates the whole program and most general program’s
Exception.
Parser- The package holds the parser class which parses the given file, and holds additional classes which
handles more ‘technical’ issues of the code.
Scopes - The package holds all scopes structure related exception, the general Scope class and it’s
inheritors. In addition, it holds the scope creator (ScopeFactory). I.e. : The ‘scopes’ package holds all
scope related classes.
Variables - similarly to scope package, the Variables package holds all variable  related classes such as
variable exception classes, Variable class, and its inheritors.
The above division to those classes was made with thinking about program’s logic structure and logic
program flow. This division creates packages which are more independent then if the division was any
different. This current division makes the packages logically and code wise separate almost completely one
from another - Which contributes code’s decomposability.

Scope and Variable classes inheritance  -
Scope inheritance -  We have decided to create a general Scope class which represents a single scope in
the given code. The general Scope class is extended by MainScope, MethodScope and ConditionalScope
(if/while scope). The Scope class holds the methods which are common to all types of scopes regardless of
their functionality.  In addition all mentioned inheriting scopes, are first of all - Scopes. Since the
relationship between all scopes and the general one  are “type of” relations, The inheriting decision was
prompted.
Variable inheritance - The variable structure has a “type of” structure as well: The variable package hold
a general Variable class. The Variable general class holds a functionality which is necessary to every
single variable. As mentioned above, every variable type is a type of “Variable”. So in current case, the
decision for inheritance was the classic move.
ScopeFactory -
After parsing all code’s lines. The program uses the ScopeFactory  class in order to divide the program
into different yet related scopes. The division is done by using a factory design pattern. The
ScopeFactory, using the given relevant information, creates a tree of scopes in which the root is the main
scope (which holds global variables). The creating of scopes is done by a factory method which returns
instances of the relevant Scope class. As we learned, Holding a factory class, contributes to codes’
single-choice and open closed principle.

VariableHandler -
VariableHandler class handles everything that a scope needs from a variable. i.e. all the scopes
requirements from a variable are handled by the VariableHandler, and so it uses as a pipe that connects
Scope and Variable. Holding a single class which connects between 2 different packages, contributes to
each package’s independency, and helps the code structure to be more decomposable. In addition, holding a
single class which connects those packages will probably ease the future programmer to change code in one
package if the original change has happened in the second one initially. I.e. holding a VariableHandler
contributes code’s continuity.



=============================
=  Implementation details   =
=============================


We did all of the line categorization and validation using regular expressions and the Pattern-Matcher
mechanism. For example we used the capturing groups in order to get the content of the brackets of an
if/while statement, or the brackets of a method declaration, or the brackets of a method call etc.
We also used regular expressions in order to make the initial parse of the file. We looked for comments
lines or for empty lines using regular expression and removed them. Afterwards we read the lines and
divided them into scopes, using a opening / closing brackets regular expressions. We implemented this
division to scopes in a recursive manner - see ScopeFactory.java .

To check things like variable assignment legality (type considerations for example), and also for
statements that uses variables specific types - like method calls and if / while statements, we used the
VariableHandler class. It is the way in which the scope can work with variables - see Design section.
To give a different type checking for each kind of type, we used the abstract method idea. Each variable
type inherit from the abstract class of Variable. This abstract class defined abstract methods to check
for valid type, and each inheritor implemented the type check according to its type. The we could use this
method in the abstract class, but when it will be called on a specific inheritor, the inheritor’s
implementation will do the actual ‘work’.

In a much similar way, we used an abstract method in the Scope abstract class. To implement different line
checking implementation for each kind of scope we have created an abstract method in the scope super class.
Each inheritor had to implement this method, and when it was called on a specific inheritor, its
implementation was the one that was executed - for example a method scope would need to validate whether
its first line is a method declaration, however a conditional scope would need to validate whether its
first line is a condition statement (if / while).

See Answers to questions, section 6.1) for exception implementations.



=============================
=    Answers to questions   =
=============================


6.1) exceptions -
To print the required number message, we used the exceptions mechanism.
In general, when a method encounter a problem that suppose to result in a compilation error of the file it
throws an exception. After the exception was thrown one of the methods in the methods calls stack, that
called it and can handle the problem (has information like: in which line number did it occur), catches the
exception. Then the catching method composes an informative message that includes the cause of the problem
and the line number, and sends the information to the main method - also using exceptions. The main method
catches a general SjavacException and prints all of the required messages.
Note that the main method also catches the IO exceptions. In any case, the main method prints out the
necessary number message which is the result of the examination of the given sjava code.


6.2.a) modifying code to add new types of variables.
Our code implementation and design makes this modification quite easy: In order to support new types of
variables, all we need to do is to insert this specific new variable type to the enum collections of types,
and to create this specific variable type class, which extends Variable class and implement its abstract
methods. Implementing Variable’s abstract methods does not requires extra work because in this abstract
methods  are necessary for each variable type in order to define its qualities.
6.2.b.1) adding Switch statement
In order to add the switch statement functionality, we will create a new Scope inheritor, SwitchScope,
which will extend the Scope class. The SwitchScope will have to implement the checkLine() method. The
SwitchScope will holds specific method which will be called in each line: the first line will have to start
to fit a regExp which will include “switch(Some statement) { “. and we will save the statement inside the
switch brackets in class’s field.
Afterwards, we will check that  each ‘block’ will start with the “case (specific case) : “ line, and will
handle the rest of the block through the variable handler. In addition, we will check that the specific
case type fits the type of the initial statement. This type issue is identical to the way we have handled
method calls parameters, and will be treated similarly. . Finally, we will check that the SwitchScope scope
is done with the “default: something”, and a closing bracket line in the end.
After creating the SwitchScope . All is left is to make the ScopeFactory support the SwitchScope class.
The ScopeFactory  will know to create a SwitchScope instance, if the first line fits (to a regexp) to a
line of type: “switch(Some statement) { “.


6.2.b.2) Importing -
To implement importing of the methods and global variables of one s-Java file to another file mechanism we
first of all need to accept an ‘import’ line in the main scope. To do this we need to add a method using
the suitable regular expression for an ‘import’ line, and use this method in the checkLine method of
MainScope.
Then we need to get a pointer to the other s-Java file. We can hold some data structure that contains all
of the files, and then search for the required file using the name we have got from the ‘import’ line by
the regexp method described above.
Then all that it is left to do is to write getters for the the variables and for the methods of MainScope
(actually for the methods we already have something similar to a getter in our code). Eventually, whenever
a search of a method call request or a search for a specific variable, we need to expand the search to the
other file if we did not find in our current file. We can easily do it using the pointer to the other file,
and the getters that we described above.
This technique can be simply used for any amount of import files - just do the search with all of the files
instead of with just one file.


6.3) Regular expressions -
6.3.1)   \s*void\s*([a-zA-Z]\w*)\s*\((.*)\)\s*\{\s*
Used in MethodScope to analyze a method declaration line.
\s* - matches any white space (quantifier: * - between zero and unlimited (greedy) ).
void - matches the characters void literally (case sensitive)
([a-zA-Z]\w*) - first capturing group.
[a-zA-Z] - match a single character from: a-z a single character in the range between a and z (case
sensitive), A-Z a single character in the range between A and Z (case sensitive).
\w* - match any word character [a-zA-Z0-9_] (quantifier: * - between zero and unlimited (greedy) ).
\( - matches the character ( literally.
(.*) - second capturing group.
.* - matches any character (except newline) (quantifier: * - between zero and unlimited (greedy) ).
\) - matches the character ) literally
\{ - matches the character { literally


6.3.2)   \s*(if|while)\s*\((\s*\w*(\s*(\|{2}|&{2})\s*\w*)*\s*)\)\s*\{\s*
Used in ConditionalScope to analyze a condition statement.
\s* - matches any white space (quantifier: * - between zero and unlimited (greedy) ).
(if|while) - first capturing group. 1st Alternative: if - matches the characters if literally (case
sensitive).  2nd Alternative: while - matches the characters while literally (case sensitive).
\( - matches the character ( literally.
(\s*\w*(\s*(\|{2}|&{2})\s*\w*)*\s*) - second capturing group.
\w* - match any word character [a-zA-Z0-9_] (quantifier: * - between zero and unlimited (greedy) ).
(\s*(\|{2}|&{2})\s*\w*)* - third capturing group (quantifier: * - between zero and unlimited (greedy) ).
(\|{2}|&{2}) - fourth Capturing group. 1st Alternative: \|{2} - matches the character | literally,
Quantifier: {2} Exactly 2 times. 2nd Alternative: &{2} - matches the characters & literally,
Quantifier: {2} Exactly 2 times.
\) - matches the character ) literally.
\{ - matches the character { literally.

