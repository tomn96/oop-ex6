package oop.ex6;

/** A generic class which represent a tuple of 2 types.
 *
 * @param <T> - The first parameter in the tuple.
 * @param <U> - The second parameter in the tuple.
 */
public class Tuple<T, U> {
    private final T t;
    private final U u;

    /**
     * Tuple class constructor.
     * @param t - The first parameter in the tuple.
     * @param u - The second parameter in the tuple.
     */
    public Tuple(T t, U u) {
        this.t = t;
        this.u = u;
    }

    /**
     * A getter.
     * @return Returns the first Parameter.
     */
    public T getFirst() {
        return t;
    }

    /**
     * A getter.
     * @return Returns the second Parameter.
     */
    public U getSecond() {
        return u;
    }
}
