package oop.ex6.main;

import oop.ex6.parser.Parser;
import oop.ex6.scopes.Scope;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * This class is a sjava code validation check (it is the main class). It prints 0 if the code is valid, 1
 * if the code is not valid or 2 if there were any problems opening the code.
 */
public class Sjavac {

    private static final int CODE_VALID = 0;
    private static final int CODE_NOT_VALID = 1;
    private static final int IO_PROBLEM = 2;

    /**
     * This is the main method that runs the sjava code validation check.
     * @param args The user's input arguments from the command line.
     */
    public static void main(String[] args) {
        try {
            File file = new File(args[0]);
            Scanner scanner = new Scanner(file); // may throw FileNotFoundException
            Scope main = Parser.parse(scanner); // may throw SjavacException
            main.checkScope(); // may throw SjavacException

            System.out.println(CODE_VALID);

        } catch (FileNotFoundException e) {
            System.out.println(IO_PROBLEM);

        } catch (SjavacException e) {
            System.err.println(e.getMessage());
            System.out.println(CODE_NOT_VALID);
        }
    }
}
