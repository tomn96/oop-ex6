package oop.ex6.main;

/**
 * This Exception is used as the most general sjava problem that our sjavac validation process has
 * discovered.
 */
public class SjavacException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * This is a constructor that initializes the exception with the given message.
     * @param message the exception's message
     */
    public SjavacException(String message) {
        super(message);
    }
}
