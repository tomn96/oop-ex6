package oop.ex6.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;

/**
 * The class is responsible of splitting and editing (slightly) lines.
 * The class holds a few methods which are responsible of 'clearing' line's suffix and split a given line
 * into tokens.
 */
public class LineEditor {

    /* A regex. Represents space string */
    private final static String SPACE_REGEX = "\\s*";

    /* A regex. Represents an equality string */
    private final static String EQUAL_REGEX = "(.*)=(.*)";

    /* A regex. Represents the ";" suffix string */
    private final static String SUFFIX_REGEX = "(.*);\\s*";

    private final static String EQUAL_STRING = "=";


    /*
     * An helper function of getLineTokens method. The method adds tokens to the token list which will be
      * returned in the getLineTokens method. The method handles a special case of 'a=b' case.
     * @param scanner - An instance of Scanner class. Scans a given line.
     * @param tokens - A list of the tokens which are constructing the line.
     */
    private static void addStatementTokens(Scanner scanner, List<String> tokens) {
        String token = scanner.next();
        Matcher equalityMatcher = RegExpHandler.regExpStringMatcher(token,EQUAL_REGEX);
        if (equalityMatcher.matches()) {
            String leftToEqualString = equalityMatcher.group(1);
            String rightToEqualString = equalityMatcher.group(2);

            if (!RegExpHandler.stringMatchesToRegExp(leftToEqualString,SPACE_REGEX)){
                tokens.add(leftToEqualString);
            }
            tokens.add(EQUAL_STRING);
            if (!RegExpHandler.stringMatchesToRegExp(rightToEqualString, SPACE_REGEX)) {
                tokens.add(rightToEqualString);
            }
        } else {
            tokens.add(token);
        }
    }

    /**
     * The method gets a string which represents a line, and divides it into line tokens.
     * @param line - A string value which represents a code line in the given file.
     * @return - A list of string, in which each string is a line token.
     */
    public static List<String> getLineTokens(String line) {

        List<String> tokens = new ArrayList<>();

        Matcher stringMatcher = RegExpHandler.regExpStringMatcher(line, "\"[^\"]*\"");

        Scanner scanner;
        int nonStringStart = 0;
        int stringStart, stringEnd;
        while (stringMatcher.find()) {
            stringStart = stringMatcher.start();
            stringEnd = stringMatcher.end();
            scanner = new Scanner(line.substring(nonStringStart,stringStart));
            while (scanner.hasNext()) {
                addStatementTokens(scanner, tokens);
            }
            tokens.add(line.substring(stringStart,stringEnd));
            nonStringStart = stringEnd;
        }
        if (nonStringStart < line.length()) {
            scanner = new Scanner(line.substring(nonStringStart,line.length()));
            while (scanner.hasNext()) {
                addStatementTokens(scanner, tokens);
            }
        }
        return tokens;
    }

    /**
     * The method is responsible of 'clearing' a line which ends with ';' suffix, from the ';' sign.
     * @param line -   A string value which represents a code line in the given file.
     * @return - The same line only without the ';' sign as a suffix.
     * @throws SuffixException - Is thrown if the line has no ';' sign has a suffix.
     */
    public static String clearLineSuffix(String line) throws SuffixException {
        Matcher suffixMatcher = RegExpHandler.regExpStringMatcher(line, SUFFIX_REGEX);
        if (suffixMatcher.matches()) {
            return suffixMatcher.group(1);
        } else {
            throw new SuffixException();
        }
    }
}
