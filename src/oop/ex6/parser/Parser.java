package oop.ex6.parser;

import oop.ex6.Tuple;
import oop.ex6.main.SjavacException;
import oop.ex6.scopes.Scope;
import oop.ex6.scopes.ScopeFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;

/**
 * The class is responsible of parsing the given sjava file. It contains a single static parse method which
 * implements the parsing process.
 */
public class Parser {

    /* A regex. Represents an empty line */
    private final static String EMPTY_LINE_REGEX = "\\s*";

    /* A regex. Represents a comment line  */
    private final static String COMMENT_PREFIX_REGEX = "\\s*//";

    /**
     * A static parsing method. The method is responsible of analyzing the a given sjava file. Using the
     * ScopeFactory, which gets an array of lines, the parse method returns a pointer to the the main scope
     * (by holding a pointer the global scope, the program potentially can access
     * to every other inner scope in the current file).
     * @param scanner - A Scanner class instance. The scanner scans all sjava file's lines.
     * @return - An instance of Scope class. The most outer scope (the global/main scope) of the current file.
     * @throws SjavacException - The method is throwing an SjavacException which it can get from ScopeFactory
     * class.
     */
    public static Scope parse(Scanner scanner) throws SjavacException {

        Matcher emptyLineMatcher = RegExpHandler.regExpStringMatcher("", EMPTY_LINE_REGEX),
                commentMatcher = RegExpHandler.regExpStringMatcher("", COMMENT_PREFIX_REGEX);

        List<Tuple<String, Integer>> lines = new ArrayList<>();
        int lineNumber = 0;

        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            lineNumber++;

            emptyLineMatcher = emptyLineMatcher.reset(nextLine);
            commentMatcher = commentMatcher.reset(nextLine);

            if (!emptyLineMatcher.matches() && !commentMatcher.lookingAt()){
                lines.add(new Tuple<>(nextLine, lineNumber));
            }
        }
        return ScopeFactory.createScope(lines);
    }
}
