package oop.ex6.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class handles with a match between a regular expression, and given string. The class contains 2 static
 * methods.
 */
public class RegExpHandler {

    /**
     * The method gets a string which represents a line a a regular expression, and using the
     * regExpStringMatcher method, returns if there is a match between the regex an the string.
     * @param line - A string. Represents a line in the sjava given file.
     * @param regExp - A String which represents a regular expression.
     * @return - true - if there is a match between the line and the regex, false otherwise.
     */
    public static boolean stringMatchesToRegExp(String line, String regExp) {
        return regExpStringMatcher(line, regExp).matches();
    }

    /**
     * The method gets a line string of the sjava file, and a regular expression. It returns An instance of
     * the Matcher class which matches the given regex and string.
     * @param line - A string. Represents a line in the sjava given file.
     * @param regExp - A String which represents a regular expression.
     * @return - An instance of Matcher class, which matches the regExp and the line.
     */
    public static Matcher regExpStringMatcher(String line, String regExp) {
        Pattern pattern = Pattern.compile(regExp);
        return pattern.matcher(line);
    }
}
