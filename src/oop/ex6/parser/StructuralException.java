package oop.ex6.parser;

import oop.ex6.main.SjavacException;

/**
 * The exception is thrown whenever a structural problem happened.
 */
public class StructuralException extends SjavacException {

    private static final long serialVersionUID = 1L;

    /**
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    public StructuralException() {
        super("Wrong scope structure");
    }

}
