package oop.ex6.parser;

import oop.ex6.scopes.ScopeException;

/**
 * An exception class which extend ScopeException. The exception is thrown whenever there is a problem with
 * the suffix of on of file's line.
 */
class SuffixException extends ScopeException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    SuffixException() {
        super("Wrong line suffix");
    }


}
