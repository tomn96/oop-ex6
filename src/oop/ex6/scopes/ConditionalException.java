package oop.ex6.scopes;

/*
 * An exception class which extends ScopeException. The exception is thrown when there is a general error
 * in the conditional (if/while) scope.
 */
class ConditionalException extends ScopeException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    ConditionalException() {
        super("General condition scope exception");
    }

    /*
     * A MethodException constructor.
     * @param message - The message to be displayed when the exception is thrown.
     */
    ConditionalException(String message) {
        super(message);
    }
}
