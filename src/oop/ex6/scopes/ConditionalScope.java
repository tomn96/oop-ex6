package oop.ex6.scopes;

import oop.ex6.Tuple;
import oop.ex6.parser.RegExpHandler;
import oop.ex6.parser.LineEditor;
import oop.ex6.variables.Variable;
import oop.ex6.variables.VariableHandler;


import java.util.List;
import java.util.regex.Matcher;

/**
 * This class represents a conditional scope in an sjava code, i.e. if or while scopes.
 */
class ConditionalScope extends Scope {

    /**
     * A constructor which initializes all of the necessary scope's fields and also the given parent scope.
     * @param parent the scope which this scope is nested in
     */
    ConditionalScope(Scope parent) {
        super(parent);
    }

    @Override
    protected void checkLine(Tuple<String, Integer> line, int lineScopeIndex) throws ScopeException {
        if (lineScopeIndex == 0) {
            if (!conditionStatement(line)) {
                throw new ConditionalException("First scope's line is not a valid conditional statement");
            }
        } else if (lineScopeIndex == lines.size()-1) {
            if (!closingBracketsStatement(line)) {
                throw new ConditionalException("Missing closing brackets at the end of method");
            }
        } else {
            if (!methodCall(line) && !returnStatement(line) && !variableDeclaration(line, false) &&
                    !variableAssignment(line, false)) {
                throw new UnhandableLineException();
            }
        }
    }

    /**
     * Checks if a line is a conditional (if / while) valid statement.
     * @param lineTuple the line to check
     * @return true if the line is a conditional valid statement, and false otherwise.
     * @throws ScopeException If there was a problem with the parameters inside the condition's brackets
     * it throws a ScopeException
     */
    private boolean conditionStatement(Tuple<String, Integer> lineTuple) throws ScopeException {
        Matcher conditionalMatcher = RegExpHandler.regExpStringMatcher(lineTuple.getFirst(),
                "\\s*(if|while)\\s*\\((\\s*\\w*(\\s*(\\|{2}|&{2})\\s*\\w*)*\\s*)\\)\\s*\\{\\s*");

        if (!conditionalMatcher.matches()){
            return false;
        }
        String conditionContent = conditionalMatcher.group(2);
        String[] statementsList = conditionContent.split("\\s*(\\|{2}|&{2})\\s*");
        if (statementsList.length == 0) {
            throw new InvalidParameterException();
        }
        for (String statement : statementsList){
            if (RegExpHandler.stringMatchesToRegExp(statement, "\\s*")) {
                throw new InvalidParameterException("Missing value in the condition");
            }
            if (!VariableHandler.checkValueRelevance(LineEditor.getLineTokens(statement),
                    Variable.Type.BOOLEAN, getVariable(statement), lineTuple.getSecond(),
                    PredicateFactory.parameterValid())){
                throw new InvalidParameterException("An attempt to put a non boolean value in an condition");
            }
        }
        return true;
    }
}
