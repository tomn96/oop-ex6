package oop.ex6.scopes;

/*
 * An exception class which extend ScopeException. The exception is thrown when one of the parameters of the
 * method call, or the content of the if/while statement is not legal.
 */
class InvalidParameterException extends ScopeException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    InvalidParameterException() {
        super("Invalid parameter");
    }

    /*
     * A MethodException constructor.
     * @param message - The message to be displayed when the exception is thrown.
     */
    InvalidParameterException(String message) {
        super(message);
    }
}
