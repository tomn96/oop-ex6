package oop.ex6.scopes;

import oop.ex6.Tuple;
import oop.ex6.main.SjavacException;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the main scope in an sjava code.
 */
class MainScope extends Scope {

    @Override
    protected void checkLine(Tuple<String, Integer> line, int lineScopeIndex) throws ScopeException {
        if (!variableDeclaration(line, true) && !variableAssignment(line, true)) {
            throw new UnhandableLineException();
        }
    }

    /**
     * This method checks for methods name duplications, and if there are none it returns a list of all
     * the methods in the main scope.
     * @return A list of all the methods in the main scope.
     * @throws MethodDuplicateException If there are method names duplications it throws a
     * MethodDuplicateException .
     */
    private List<MethodScope> getMethods() throws MethodDuplicateException {
        List<MethodScope> methods = new ArrayList<>();
        for (Scope child : children) {
            MethodScope method = (MethodScope) child; /* all of MainScope inner scopes are MethodScope by
            definition in sjava */

            if(methods.stream().map(MethodScope::getName).anyMatch(s1 -> s1.equals(method.getName()))) {
                throw new MethodDuplicateException("Method name \"" + method.getName() +
                        "\" been declared already");
            } else {
                methods.add(method);
            }
        }
        return methods;
    }

    @Override
    public void checkScope() throws SjavacException {
        super.checkScope();

        try {
            checkMethodRequests(getMethods());
        } catch (MethodDuplicateException e) {
            throw new SjavacException(e.getMessage());
        }
    }
}
