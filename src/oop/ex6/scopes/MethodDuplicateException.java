package oop.ex6.scopes;

/*
 * An exception class which extend MethodException. The exception is thrown when the program identifies 2
  * methods with the same name.
 */
class MethodDuplicateException extends MethodException {

    private static final long serialVersionUID = 1L;

    /*
     * A MethodException constructor.
     * @param message - The message to be displayed when the exception is thrown.
     */
    MethodDuplicateException(String message) {
        super(message);
    }
}
