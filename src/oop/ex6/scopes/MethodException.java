package oop.ex6.scopes;

/*
 * An exception class which extend ScopeException. The exception is thrown when there is a general problem
 * which is related to the method scope.
 */
class MethodException extends ScopeException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    MethodException() {
        super("General method exception");
    }

    /*
     * A MethodException constructor.
     * @param message - The message to be displayed when the exception is thrown.
     */
    MethodException(String message) {
        super(message);
    }
}
