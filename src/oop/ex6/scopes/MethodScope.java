package oop.ex6.scopes;


import oop.ex6.Tuple;
import oop.ex6.main.SjavacException;
import oop.ex6.parser.RegExpHandler;
import oop.ex6.variables.Variable;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * This class represents a method scope in an sjava code.
 */
class MethodScope extends Scope {

    /** A list of the method parameter's types by order */
    private List<Variable.Type> types;

    /** The method's name */
    private String name;

    /**
     * A constructor that initializes all of the necessary scope's fields and also the given parent scope.
     * It also initializes an empty types list.
     * @param parent the scope which this scope is nested in
     */
    MethodScope(Scope parent) {
        super(parent);
        types = new ArrayList<>();
    }

    /**
     * Checks if a line is a valid method declaration statement.
     * @param lineTuple the line to check
     * @return true if the line is a valid method declaration statement, and false otherwise.
     * @throws ScopeException If there was a problem with the parameters inside the method's brackets
     * it throws a ScopeException
     */
    private boolean voidStatement(Tuple<String, Integer> lineTuple) throws ScopeException {

        Matcher methodMatcher = RegExpHandler.regExpStringMatcher(lineTuple.getFirst(),
                "\\s*void\\s*([a-zA-Z]\\w*)\\s*\\((.*)\\)\\s*\\{\\s*");

        if (methodMatcher.matches()) {
            name = methodMatcher.group(1);

            String methodParameters = methodMatcher.group(2);
            String[] parameters = methodParameters.split("\\s*,\\s*");

            if (parameters.length == 1 && RegExpHandler.stringMatchesToRegExp(parameters[0],"\\s*")) {
                return true;
            }

            for (String parameter : parameters) {
                List<Variable> newVariables = variableDeclarationHelper(new Tuple<>(parameter,
                        lineTuple.getSecond()), false, PredicateFactory.parameterValid());
                if (newVariables == null || newVariables.size() != 1) {
                    throw new InvalidParameterException();
                } else {
                    Variable newVariable = newVariables.get(0);
                    newVariable.setInitialized(lineTuple.getSecond());
                    types.add(newVariable.getType());
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * A getter for the method's name
     * @return The method's name
     */
    String getName() {
        return name;
    }

    /**
     * A getter for the method's parameters types list
     * @return the method's parameters types list
     */
    List<Variable.Type> getTypes() {
        return types;
    }

    @Override
    protected void checkLine(Tuple<String, Integer> line, int lineScopeIndex) throws ScopeException {
        if (lineScopeIndex == 0) {
            if (!voidStatement(line)) {
                throw new MethodException("First scope's line isn't a valid method declaration statement");
            }
        } else if (lineScopeIndex == lines.size()-2) {
            if (!returnStatement(line)) {
                throw new MethodException("Missing return statement at the end of method");
            }
        } else if (lineScopeIndex == lines.size()-1) {
            if (!closingBracketsStatement(line)) {
                throw new MethodException("Missing closing brackets at the end of method");
            }
        } else {
            if (!methodCall(line) && !returnStatement(line) && !variableDeclaration(line, false) &&
                    !variableAssignment(line, false)) {
                throw new UnhandableLineException();
            }
        }
    }

    @Override
    public void checkScope() throws SjavacException {
        resetGlobalVariables();
        super.checkScope();
    }
}
