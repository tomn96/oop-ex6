package oop.ex6.scopes;

/**
 * An exception class which extend ScopeException. The exception is thrown when the statement is not readable
 * in the structural aspect.
 */
public class NotReadableStatementException extends ScopeException {

    private static final long serialVersionUID = 1L;

    /**
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    public NotReadableStatementException() {
        super("Statement not written in a valid pattern");
    }

}
