package oop.ex6.scopes;


import java.util.List;
import java.util.function.BiPredicate;

/**
 * This class is a factory for predicates that check low level validation of statements like syntax fit
 * etc.
 */
class PredicateFactory {

    private static final String EQUALS_SIGN = "=";
    private static final int ONE_TOKEN_STATEMENT = 1;
    private static final int THREE_TOKENS_STATEMENT = 3;

    /**
     * @return this method returns a predicate that checks a statement related to variable declaration.
     */
    static BiPredicate<List<String>, Boolean> variableDeclarationValid() {

        return (strings, aBoolean) -> {
            if (strings.size() == ONE_TOKEN_STATEMENT || strings.size() == THREE_TOKENS_STATEMENT) {
                if (aBoolean) { // if it is a final declaration we must require and assignment
                    return assignmentLineValid().test(strings, aBoolean);
                } else {
                    return strings.size() == ONE_TOKEN_STATEMENT || strings.get(1).equals(EQUALS_SIGN);
                }
            }
            return false;
        };
    }

    /**
     * @return this method returns a predicate that checks a statement related to parameters such as in a
     * method call or in a condition scope.
     */
    static BiPredicate<List<String>, Boolean> parameterValid() {
        return (strings, aBoolean) -> strings.size() == ONE_TOKEN_STATEMENT;
    }

    /**
     * @return this method returns a predicate that checks a statement related to variable assignment.
     */
    static BiPredicate<List<String>, Boolean> assignmentLineValid() {
        return (strings, aBoolean) ->
                strings.size() == THREE_TOKENS_STATEMENT && strings.get(1).equals(EQUALS_SIGN);
    }
}
