package oop.ex6.scopes;

import oop.ex6.Tuple;
import oop.ex6.main.SjavacException;
import oop.ex6.parser.RegExpHandler;
import oop.ex6.parser.LineEditor;
import oop.ex6.variables.Variable;
import oop.ex6.variables.VariableHandler;


import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.regex.Matcher;

/**
 * This class is an abstract class represents a scope in an sjava code.
 */
public abstract class Scope {

    private static final String FINAL = "final";
    private static final String EQUALS_SIGN = "=";

    /** All the lines specific to this scope */
    protected List<Tuple<String, Integer>> lines;
    /** All the variables which are specific to this scope */
    private List<Variable> variables;

    /** The scope which this scope is nested in */
    private Scope parent;
    /** All the scopes nested in this scope */
    protected List<Scope> children;

    /** All the attempts that were maid to call a method within this scope */
    private List<Tuple<String, Integer>> methodCallsRequests;

    /**
     * A constructor which initializes all of the necessary scope's fields and also the given parent scope.
     * @param parent the scope which this scope is nested in
     */
    Scope(Scope parent) {
        this.lines = new ArrayList<>();
        this.variables = new ArrayList<>();

        this.parent = parent;
        this.children = new ArrayList<>();

        this.methodCallsRequests = new ArrayList<>();

    }

    /**
     * A constructor which initializes all of the necessary scope's fields, and the parent field to null.
     */
    Scope() {
        this(null);
    }

    /**
     * adds line to the scope's lines list
     * @param line the line to add to the list
     */
    void addLine(Tuple<String, Integer> line) {
        lines.add(line);
    }

    /**
     * adds a line to the start of the 'lines' list
     * @param line the line to be added to the beginning of the list.
     */
    void addFirstLine(Tuple<String, Integer> line) {
        this.lines.add(0, line); // adds the line to the start (to index 0).
    }

    /**
     * A geter for the 'parent' scope.
     * @return The scope which this scope is nested in
     */
    Scope getParent() {
        return parent;
    }

    /**
     * Adds a scope to list of 'children' which holds the scopes nested in this scope.
     * @param child the inner scope to add to the children list
     */
    void addChild(Scope child) {
        children.add(child);
    }

    /**
     * Gets a variable in the given name. It looks for the variable in all the reachable places by this
     * scope, i.e. in all the parent scope of this class as well as in this specific class.
     * @param name the name of the variable to look for
     * @return the variable which has the given name, or null if no such variable exists in the reachable
     * areas of this scope.
     */
    protected Variable getVariable(String name) {
        for (Variable variable : variables) {
            if (variable.getName().equals(name)) {
                return variable;
            }
        }

        if (parent != null) {
            return parent.getVariable(name);
        }
        return null;
    }

    /**
     * Checks if this scope already contain a variable which holds the given name.
     * @param varName the name to check if there is already a variable with this name in this scope.
     * @throws VariableDuplicateException if there is already a variable with the given name it throws a
     * VariableDuplicateException.
     */
    private void checkVariableDuplicates(String varName) throws VariableDuplicateException {
        for (Variable variable : variables) {
            if (variable.getName().equals(varName)) {
                throw new VariableDuplicateException();
            }
        }
    }

    /**
     * This method takes a line, reaches all of its variable declarations statements and uses the
     * VariableHandler to create the necessary variables to this scope.
     * @param lineTuple a line that may contain variable declarations.
     * @param global whether this line was in a global scope or not.
     * @param predicate a predicate that determines whether each declaration is valid. It only for base
     *                  checks like syntax and etc.
     * @return a list of the new variables that were declared in this line and created by this method
     * actions, or null if the line did not fit to the declaration syntax.
     * @throws ScopeException if there were a problem generating any new variable or if there was a name
     * duplication it throws a ScopeException.
     */
    protected List<Variable> variableDeclarationHelper(Tuple<String, Integer> lineTuple, boolean global,
                                                       BiPredicate<List<String>, Boolean> predicate)
                                                        throws ScopeException {
        String line = lineTuple.getFirst();

        String[] statementsArray = line.split("\\s*,\\s*"); // split all the statements in the line

        boolean finalFlag = false; // whether there is a final keyword in the beginning of the declaration.
        Variable.Type type; // the type of the variables declared.

        List<List<String>> statementTokensList = new ArrayList<>();
        for (String statement : statementsArray) {
            statementTokensList.add(LineEditor.getLineTokens(statement));
        }

        if (statementTokensList.size() == 0 || statementTokensList.get(0).size() == 0) {
            return null;
        }

        if (statementTokensList.get(0).get(0).equals(FINAL)) {
            finalFlag = true;
            statementTokensList.get(0).remove(0); // removes the 'final' token
            if (statementTokensList.get(0).size() == 0) {
                return null;
            }
        }

        try {
            type = Variable.Type.value(statementTokensList.get(0).get(0));
        } catch (IllegalArgumentException e) {
            return null;
        }

        statementTokensList.get(0).remove(0);

        List<Variable> newVariables = new ArrayList<>();
        Variable newVariable, assignmentVariable;

        for (List<String> statementTokens : statementTokensList) {
            assignmentVariable = null;
            if (statementTokens.size() == 3) {
                assignmentVariable = getVariable(statementTokens.get(2));
            }
            if (statementTokens.size() > 0) {
                checkVariableDuplicates(statementTokens.get(0));
            }
            newVariable = VariableHandler.createVariableAndCheckStatement(statementTokens, type, finalFlag,
                    assignmentVariable, lineTuple.getSecond(), global, predicate);
            newVariables.add(newVariable);
            variables.add(newVariable);
        }
        return newVariables;
    }

    /**
     * This method takes a line, checks if it has a correct suffix, and then uses variableDeclarationHelper
     * to reach all of its variable declarations statements and to create the necessary variables for this
     * scope.
     * @param lineTuple a line that may contain variable declarations.
     * @param global whether this line was in a global scope or not.
     * @return true if it was a variable declaration line, and false otherwise.
     * @throws ScopeException If there was a problem with the variableDeclarationHelper or if the line did
     * not contain the correct suffix it throws a ScopeException.
     */
    protected boolean variableDeclaration(Tuple<String, Integer> lineTuple, boolean global)
            throws ScopeException {
        String line = LineEditor.clearLineSuffix(lineTuple.getFirst()); // may throw exception
        return variableDeclarationHelper(new Tuple<>(line, lineTuple.getSecond()), global,
                PredicateFactory.variableDeclarationValid()) != null;
    }

    /**
     * Checks if a line is an assignment to a variable line, and if it does it make sure to update all of
     * the necessary fields.
     * @param lineTuple a line that may be a line of an assignment to a variable.
     * @param global whether this line was in a global scope or not.
     * @return true if it was a variable assignment line, and false otherwise.
     * @throws ScopeException If there was a problem with the VariableHandler or if the line did not
     * contain the correct suffix it throws a ScopeException.
     */
    protected boolean variableAssignment(Tuple<String, Integer> lineTuple, boolean global)
            throws ScopeException {
        String line = LineEditor.clearLineSuffix(lineTuple.getFirst()); // may throw exception

        List<String> tokens = LineEditor.getLineTokens(line);
        if (tokens.size() == 3) {
            Variable subjectVariable = getVariable(tokens.get(0));

            if (subjectVariable != null && tokens.get(1).equals(EQUALS_SIGN)) {

                Variable settingVariable = getVariable(tokens.get(2));
                return VariableHandler.checkValueAssignment(tokens, subjectVariable, settingVariable,
                        lineTuple.getSecond(), global, PredicateFactory.assignmentLineValid()); /*
                         may produce exception */
            }
        }
        return false;
    }

    /**
     * Checks if a line is a 'return' statement.
     * @param lineTuple the line to check
     * @return true if the line is a return statement, and false otherwise.
     */
    protected static boolean returnStatement(Tuple<String, Integer> lineTuple) {
        return RegExpHandler.stringMatchesToRegExp(lineTuple.getFirst(), "\\s*return\\s*;\\s*");
    }

    /**
     * Checks if a line is a closing brackets statement.
     * @param lineTuple the line to check
     * @return true if the line is a closing brackets statement, and false otherwise.
     */
    protected static boolean closingBracketsStatement(Tuple<String, Integer> lineTuple) {
        return RegExpHandler.stringMatchesToRegExp(lineTuple.getFirst(), "\\s*}\\s*");
    }

    /**
     * Checks if a line may be a call to a method statement.
     * @param lineTuple the line to check
     * @return true if the line may be a call to a method statement, and false otherwise.
     */
    protected boolean methodCall(Tuple<String, Integer> lineTuple) {
        Matcher methodCallMatcher = RegExpHandler.regExpStringMatcher(lineTuple.getFirst(),
                "\\s*([a-zA-Z]\\w*)\\s*\\((.*)\\)\\s*;\\s*");
        if (methodCallMatcher.matches()) {
            methodCallsRequests.add(lineTuple);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds a method by the given name in the given methods list and returns it.
     * @param methodName the name of the method to look for.
     * @param methods the list of methods to search in for the method name.
     * @return the method that has the given method name
     * @throws UnknownMethodCallException if there is no method with the given method name in the given
     * methods list it throws an UnknownMethodCallException.
     */
    private static MethodScope findMethod(String methodName, final List<MethodScope> methods)
            throws UnknownMethodCallException {
        for (MethodScope method : methods) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        throw new UnknownMethodCallException();
    }

    /**
     * Checks if a given line was actually a valid method call.
     * @param lineTuple The line to check if it was a valid method call.
     * @param methods The list of methods that can be called
     * @throws ScopeException If there was an error with the allegedly method call it throws a
     * ScopeException.
     */
    private void checkSpecificMethodRequest(Tuple<String, Integer> lineTuple,
                                            final List<MethodScope> methods) throws ScopeException {

        Matcher methodCallMatcher = RegExpHandler.regExpStringMatcher(lineTuple.getFirst(),
                "\\s*([a-zA-Z]\\w*)\\s*\\((.*)\\)\\s*;\\s*");
        if (!methodCallMatcher.matches()) {
            throw new MethodException();
        }

        String methodName = methodCallMatcher.group(1);
        String stringParameters = methodCallMatcher.group(2);
        String[] parameters = stringParameters.split("\\s*,\\s*");

        MethodScope method = findMethod(methodName, methods);

        if (method.getTypes().size() == 0 && parameters.length == 1 &&
                RegExpHandler.stringMatchesToRegExp(parameters[0], "\\s*")){
            return;
        } else if (method.getTypes().size() == parameters.length) {
            for (int i = 0; i < parameters.length; i++) {
                if (!VariableHandler.checkValueRelevance(LineEditor.getLineTokens(parameters[i]),
                        method.getTypes().get(i), getVariable(parameters[i]),
                        lineTuple.getSecond(), PredicateFactory.parameterValid())) {
                    throw new InvalidParameterException();
                }
            }
        } else {
            throw new UnknownMethodCallException();
        }
    }

    /**
     * Checks all of the scope's method calls and also the scope's children's method calls.
     * @param methods The list of methods that can be called
     * @throws SjavacException If there were an error with the checkSpecificMethodRequest it throws a
     * SjavacException.
     */
    protected void checkMethodRequests(final List<MethodScope> methods) throws SjavacException {
        for (Tuple<String, Integer> lineTuple : methodCallsRequests) {
            try {
                checkSpecificMethodRequest(lineTuple, methods);
            } catch (ScopeException e) {
                throw new SjavacException("Error in line " + lineTuple.getSecond() + ": "  + e.getMessage());
            }
        }

        for (Scope child : children) {
            child.checkMethodRequests(methods);
        }
    }

    /**
     * Resets all of the global variables to their global state. All the variables meaning is all the
     * variables that this method has access to, i.e. also the parent's variables.
     */
    protected void resetGlobalVariables() {
        variables.forEach(Variable::resetIfGlobal);

        if (parent != null) {
            parent.resetGlobalVariables();
        }
    }

    /**
     * This method checks a given line to see if it fits an allowed line in the sjava conventions.
     * @param line the line to check if it is allowed.
     * @param lineScopeIndex The line's index relative to the scope's lines.
     * @throws ScopeException If there was a problem with the line it throws a ScopeException.
     */
    protected abstract void checkLine(Tuple<String, Integer> line, int lineScopeIndex)
            throws ScopeException;

    /**
     * Checks if the scope is valid according to the sjava conventions. It also checks for all of the inner
     * scopes (children) validation.
     * @throws SjavacException if there was a problem with one of the lines in the scope it throws a
     * SjavacException.
     */
    public void checkScope() throws SjavacException {

        for (int i=0; i < lines.size(); i++) {
            try {
                checkLine(lines.get(i), i);
            } catch (ScopeException e) {
                throw new SjavacException("Error in line " + lines.get(i).getSecond() + ": "  +
                        e.getMessage());
            }
        }

        for (Scope child : children) {
            child.checkScope();
        }
    }
}
