package oop.ex6.scopes;

import oop.ex6.main.SjavacException;

/**
 * The exception is thrown when there is a general Scope error.
 */
public class ScopeException extends SjavacException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    public ScopeException() {
        super("General scope exception");
    }

    /**
     * A ScopeException constructor.
     * @param message - The message to be displayed when the exception is thrown.
     */
    public ScopeException(String message) {
        super(message);
    }
}
