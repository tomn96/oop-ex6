package oop.ex6.scopes;

import oop.ex6.Tuple;
import oop.ex6.main.SjavacException;
import oop.ex6.parser.RegExpHandler;
import oop.ex6.parser.StructuralException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * The ScopeFactory class is responsible of getting a list of code lines and separate it into scopes which
 * are instances of Scope class. Each Scope instance has it's own scope's lines, a pointer to his parent
 * (the outer scope) and  a pointer to his children (his inner scopes).
 */
public class ScopeFactory {

    /*  A regular expression. Represents an opening bracket string. */
    private final static String OPEN_BRACKET_REGEX = ".*\\{\\s*";

    /*  A regular expression. Represents an closing bracket string. */
    private final static String CLOSE_BRACKET_REGEX =  ".*}\\s*";

    /*
     * An nested class of ScopeFactory class. This class holds three generic elements.
     */
    private static class Triple<T, U, V> {
        private final T t;
        private final U u;
        private final V v;

        /*
         * A constructor of the generic Triple class.
         * @param t - The first element of the triple.
         * @param u - The second element of the triple.
         * @param v - The third element of the triple.
         */
        private Triple(T t, U u, V v) {
            this.t = t;
            this.u = u;
            this.v = v;
        }

        /* Returns the first element of the triple */
        private T getFirst() {
            return t;
        }

        /* Returns the second element of the triple*/
        private U getSecond() {
            return u;
        }

        /* Returns the third element of the triple*/
        private V getThird() {
            return v;
        }
    }

    /*
     * A recursive helper function of createScope method. Creates Scopes data structure ( see createScope
     * documentation).
     * @param lines - A list of tuples in which the first element is each line's content, and the second
     *              represents the number of the line.
     * @param parent - A Scope instance The parent of the current Scope instance (the outer scope)
     * @param start - The first line in which the current scope begins in.
     * @param bracketsDifference - An int. Represents the total difference between the amount of the opening
     * and closing brackets.
     * @return - A triple class instance. in which the first element is a Scope instance, the second element
     * represents the line number in which the scope begins.
      * is and the third represents a total amount of (openBracket - closeBracket) in code.
     * @throws StructuralException - Thrown if there was a problem with code's general structure.
     */
    private static Triple<Scope, Integer, Integer> createScopeHelper(List<Tuple<String, Integer>> lines,
                                                                     Scope parent, int start,
                                                                     int bracketsDifference)
            throws StructuralException {

        Scope scope;
        if (parent == null) {
            scope = new MainScope();
        } else if (parent.getParent() == null) {
            scope = new MethodScope(parent);
        } else {
            scope = new ConditionalScope(parent);
        }

        Matcher openBracketMatcher = RegExpHandler.regExpStringMatcher("", OPEN_BRACKET_REGEX),
                closeBracketMatcher = RegExpHandler.regExpStringMatcher("", CLOSE_BRACKET_REGEX);
        int bracketsCounter = bracketsDifference;

        int lineNumber = start;
        while (lineNumber < lines.size()) {

            String line = lines.get(lineNumber).getFirst();
            openBracketMatcher.reset(line);
            closeBracketMatcher.reset(line);

            if (openBracketMatcher.matches()) {
                bracketsCounter++;
                Triple<Scope, Integer, Integer> childTriple = createScopeHelper(lines, scope, lineNumber+1,
                        bracketsCounter);

                Scope child = childTriple.getFirst();
                child.addFirstLine(lines.get(lineNumber));
                scope.addChild(child);

                lineNumber = childTriple.getSecond();
                bracketsCounter = childTriple.getThird();

            } else if (closeBracketMatcher.matches()) {
                bracketsCounter--;
                scope.addLine(lines.get(lineNumber));
                lineNumber++;
                return new Triple<>(scope, lineNumber, bracketsCounter);
            } else {
                scope.addLine(lines.get(lineNumber));
                lineNumber++;
            }
        }
        if (bracketsCounter == 0) {
            return new Triple<>(scope, -1, -1);
        } else {
            throw new StructuralException();
        }
    }

    /**
     * The method gets a list of all code's lines. Using the createScopeHelper method, it returns the main
     * scope as an instance of Scope class, and in fact, it returns a pointer the the whole code which is
     * represented by a data structure of scopes (a tree with the main scope as it's root).
     * @param lines - A list of tuples in which the first element is each line's content, and the second
     *              represents the number of the line.
     * @return - An instance of Scope class - the main (global) scope.
     * @throws SjavacException - Thrown if there was a problem with scope creation process.
     */
    public static Scope createScope(List<Tuple<String, Integer>> lines) throws SjavacException {
        try {
            return createScopeHelper(lines, null, 0, 0).getFirst();
        } catch (StructuralException e) {
            throw new SjavacException(e.getMessage());
        }
    }
}
