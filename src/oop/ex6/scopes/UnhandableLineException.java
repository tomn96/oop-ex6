package oop.ex6.scopes;

/*
 * An exception class which extend ScopeException. The exception is thrown when the program doesn't know
 * how to handle or 'classify' a certain line.
 */
class UnhandableLineException extends ScopeException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    UnhandableLineException() {
        super("Cannot handle current line");
    }

}

