package oop.ex6.scopes;

/*
 * An exception class which extend MethodException. The exception is thrown when there is a method call to a
 * non exciting method.
 */
class UnknownMethodCallException extends MethodException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    UnknownMethodCallException() {
        super("An attempt to call an undefined method");
    }


}
