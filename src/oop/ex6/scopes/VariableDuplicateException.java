package oop.ex6.scopes;

/*
 * An exception class which extend ScopeException. The exception is thrown whenever the program encounters
 * with a declared variable which has an already taken variable name.
 */
class VariableDuplicateException extends ScopeException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    VariableDuplicateException() {
        super("Variable name duplication");
    }

}
