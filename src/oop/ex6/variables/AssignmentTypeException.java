package oop.ex6.variables;

/*
 * An exception class which extends VariableException. The exception is thrown whenever the type of the
 * assignment value/variable does not fits the type of the declared variable.
 */
class AssignmentTypeException extends VariableException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    AssignmentTypeException() {
        super("An attempt to assign a non legal type");
    }

}
