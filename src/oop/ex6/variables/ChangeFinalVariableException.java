package oop.ex6.variables;

/*
 * An exception class which extends VariableException. The exception is thrown whenever there is an attempt to
 * change final variable's assignment.
 */
class ChangeFinalVariableException extends VariableException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    ChangeFinalVariableException() {
        super("Changing a final variable is forbidden");
    }

}
