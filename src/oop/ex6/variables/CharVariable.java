package oop.ex6.variables;

import oop.ex6.parser.RegExpHandler;

/*
 * A variable which represents a character variable. Extends Variable class.
 */
class CharVariable extends Variable {

    /* A regex. Represents a single character string. */
    private final static String CHAR_REGEX = "'.?'";

    /*
     * A CharVariable constructor.
     */
    CharVariable(Type type, String name, boolean global, boolean finalFlag, int lineDeclared)
            throws VariableException {
        super(type, name, global, finalFlag, lineDeclared);
    }

    @Override
    protected void setValueCheck(String value) throws AssignmentTypeException {

        if (!RegExpHandler.stringMatchesToRegExp(value, CHAR_REGEX)){
            throw new AssignmentTypeException();
        }
    }

    @Override
    protected void setVariableCheck(Variable variable) throws AssignmentTypeException {
        if (!(variable!= null && variable.type.equals(Type.CHAR))){
            throw new AssignmentTypeException();
        }
    }
}
