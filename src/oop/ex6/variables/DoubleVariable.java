package oop.ex6.variables;

/*
 * A variable which represents a double variable. Extends Variable class.
 */
class DoubleVariable extends Variable {

    /*
     * A DoubleVariable constructor.
     */
    DoubleVariable(Type type, String name, boolean global, boolean finalFlag, int lineDeclared)
            throws VariableException {
        super(type, name, global, finalFlag, lineDeclared);
    }

    @Override
    protected void setValueCheck(String value) throws AssignmentTypeException {
        try{
            Double num = Double.parseDouble(value);
        } catch (NumberFormatException e){
            throw new AssignmentTypeException();
        }
    }

    @Override
    protected void setVariableCheck(Variable variable) throws AssignmentTypeException {

        if(!(variable != null && (variable.type.equals(Type.INT) || variable.type.equals(Type.DOUBLE)))){
            throw new AssignmentTypeException();
        }
    }
}
