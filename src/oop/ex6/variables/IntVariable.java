package oop.ex6.variables;

/*
 * A variable which represents an int variable. Extends Variable class.
 */
class IntVariable extends Variable {

    /*
     * An IntVariable constructor.
     */
    IntVariable(Type type, String name, boolean global, boolean finalFlag, int lineDeclared) throws
            VariableException {
        super(type, name, global, finalFlag, lineDeclared);
    }

    @Override
    protected void setValueCheck(String value) throws AssignmentTypeException {
        try{
            int num = Integer.parseInt(value);
        } catch (NumberFormatException e){
            throw new AssignmentTypeException();
        }
    }

    @Override
    protected void setVariableCheck(Variable variable) throws AssignmentTypeException {
        if(!(variable != null && variable.type.equals(Type.INT))){
            throw new AssignmentTypeException();
        }
    }
}
