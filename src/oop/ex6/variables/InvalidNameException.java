package oop.ex6.variables;

/*
 * An exception class which extends VariableException. The exception is thrown if the name of the declared
 * variable is not legal.
 */
class InvalidNameException extends VariableException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    InvalidNameException() {
        super("Invalid variable name");
    }

}
