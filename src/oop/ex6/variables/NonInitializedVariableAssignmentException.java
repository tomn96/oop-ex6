package oop.ex6.variables;

/*
 * An exception class which extends VariableException. The exception is thrown if there is an attempt to
 * assign a value/variable to a variable which has never been declared.
 */
class NonInitializedVariableAssignmentException extends VariableException {

    private static final long serialVersionUID = 1L;

    /*
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    NonInitializedVariableAssignmentException() {
        super("An attempt to assign a non initialized variable");
    }

}
