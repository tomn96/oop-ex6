package oop.ex6.variables;

import oop.ex6.parser.RegExpHandler;

/*
 * A variable which represents a string type variable. Extends Variable class.
 */
class StringVariable extends Variable {

    /* A regex. Represents a String type value. */
    private final static String STRING_REGEX = "\".*\"";

    /*
     * A StringVariable constructor.
     */
    StringVariable(Type type, String name, boolean global, boolean finalFlag, int lineDeclared)
            throws VariableException {
        super(type, name, global, finalFlag, lineDeclared);
    }

    @Override
    protected void setValueCheck(String value) throws AssignmentTypeException {
        if(!RegExpHandler.stringMatchesToRegExp(value, STRING_REGEX)){
            throw new AssignmentTypeException();
        }

    }

    @Override
    protected void setVariableCheck(Variable variable) throws AssignmentTypeException {
        if(!(variable != null && variable.type.equals(Type.STRING))){
            throw new AssignmentTypeException();
        }
    }
}
