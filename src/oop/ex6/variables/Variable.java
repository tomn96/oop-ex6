package oop.ex6.variables;

import oop.ex6.parser.RegExpHandler;
import java.util.regex.Matcher;

/**
 * An abstract class which represents a single variable. The variable has a few qualities which fits a regular
 * variable, such as: is global, is initialized, is final etc. If a variable was initialized, Variable class
 * has methods which will check if the initialization if legal.
 */
public abstract class Variable {

    /** An enum. Represents the possible types of variables. */
    public enum Type {INT, DOUBLE, STRING, BOOLEAN, CHAR;

        /**
         * returns the type suitable for the string representation.
         * @param s the string representation of the required type
         * @return the type suitable to the given string
         */
        public static Type value(String s) {
            switch (s) {
                case "int":
                    return INT;
                case "double":
                    return DOUBLE;
                case "String":
                    return STRING;
                case "boolean":
                    return BOOLEAN;
                case "char":
                    return CHAR;
                default:
                    throw new IllegalArgumentException(); // run time exception
            }
        }
    }

    /** An instance of Type class.*/
    protected Type type;

    /* A regular expression. Represents a legal name of variable. */
    private final static String LEGAL_VAR_NAME_REGEX = "_\\w+|[a-zA-Z]\\w*";

    /* A string. Represents a name of a variable constant.  */
    private String name;

    /* A boolean which represents whether the variable if final or not. */
    private boolean finalFlag;

    /* An int which states the line in which the variable has declared.  */
    private int lineDeclared;

    /* An int which states the last line in which the variable has been initialized.  */
    private int lineInitialized;

    /* A boolean which represents whether the variable was declared in a global scope or not.  */
    private boolean global;

    /* An int which states the last global line in which the variable has been initialized.  */
    private int globalInitializationLine;


    /**
     * A constructor of the Variable class. Creates an instance of the current class.
     * @param type - The type of the variable.
     * @param name - A string. Represents the name of the variable.
     * @param global - A flag which indicates whether the variable was declared in a global scope/
     * @param finalFlag -  A boolean which represents whether the variable if final or not.
     * @param lineDeclared - An int which states the line in which the variable has declared.
     * @throws VariableException - Thrown if the name of the variable is not legal.
     */
    protected Variable(Type type, String name, boolean global, boolean finalFlag, int lineDeclared) throws
            VariableException {

        checkName(name); // throws exception

        this.type = type;
        this.name = name;
        this.finalFlag = finalFlag;
        this.lineDeclared = lineDeclared;
        this.lineInitialized = -1;

        this.global = global;
        this.globalInitializationLine = -1;
    }

    /*
     * The method checks whether the given name of the variable is a legal name.
     * @param name - A string. Represents a name of a variable.
     * @throws InvalidNameException - Thrown if the name of the variable is not legal.
     */
    private static void checkName(String name) throws InvalidNameException {
        Matcher validNameMatcher = RegExpHandler.regExpStringMatcher(name, LEGAL_VAR_NAME_REGEX);

        if (!validNameMatcher.matches()) {
            throw new InvalidNameException();
        }
    }

    /**
     * The method sets a given value to a declared variable. In addition, it checks whether the current value
     * initialization is legal ( The type of the value fits the declared variable).
     * @param value - The future value of the declared variable.
     * @throws AssignmentTypeException -  Thrown if the type of the given value does not fit variable's type.
     */
    protected abstract void setValueCheck(String value) throws AssignmentTypeException;

    /**
     * The method sets a given variable to a declared variable. In addition, it checks whether the current
     * variable initialization is legal (The type of the value fits the declared variable).
     * @param variable - A variable (Variable object), which will be initialized to the declared variable.
     * @throws AssignmentTypeException -  Thrown if the type of the given variable does not fit
     * declared variable's type.
     */
    protected abstract void setVariableCheck(Variable variable) throws AssignmentTypeException;

    /*
     * A setter. The method is responsible of assigning a value/variable to the current declared variable.
     * The method is both functional for setting a value or a variable.
     * @param value - A possible value which will be assigned to the current variable.
     * @param settingVariable - A possible variable which will be assigned to the current variable.
     * @param line - The line in which the variable has been assigned.
     * @param lineIsGlobal - A flag which indicates whether the assigning line is a global scope line.
     * @throws VariableException - Thrown when the vale/variable setting did not succeed.
     */
    void setValue(String value, Variable settingVariable, int line, boolean lineIsGlobal)
            throws VariableException {
        if (finalFlag && line != lineDeclared) {
            throw new ChangeFinalVariableException();
        }

        if (settingVariable != null) {
            if (settingVariable.lineInitialized < 0){
                throw new NonInitializedVariableAssignmentException(); // SettingVariable never initialized
            } else if (!settingVariable.global && settingVariable.lineInitialized > line){
                // SettingVariable is not global but was initialized after current line.
                throw new NonInitializedVariableAssignmentException();
            } else if (lineIsGlobal && settingVariable.global && settingVariable.lineInitialized > line){
                // Surrent var' is global and settingVariable is global but settingVariable wasn't assign yet.
                throw new NonInitializedVariableAssignmentException();

            }
            setVariableCheck(settingVariable); // throws exception
        } else {
            setValueCheck(value); // throws exception
        }
        setInitialized(line, lineIsGlobal);
    }

    /**
     * A getter. Gets the name of the current variable.
     * @return - The name of the current variable.
     */
    public String getName() {
        return name;
    }

    /**
     * A getter. Gets the type of the current variable.
     * @return - The type (Type object) of the current variable.
     */
    public Type getType() {
        return type;
    }

    /*
     * A private setter. The method sets the line in which the variable have been initialized.
     * @param lineInitialized - The line in which the variable lately initialized.
     * @param global - A boolean. whether the current set is in global or local scope.
     */
    private void setInitialized(int lineInitialized, boolean global) {
        this.lineInitialized = lineInitialized;
        if (global) {
            this.globalInitializationLine = lineInitialized;
        }
    }

    /**
     * If a global variable was initialized in a local scope, and the program continued running ofter current
     * local scope, the method will reset the line in which the variable been initialized back to the global
     * scope initialization line.
     */
    public void resetIfGlobal() {
        if (global) {
            lineInitialized = globalInitializationLine;
        }
    }

    /**
     * A setter. The method sets the line in which the variable have been initialized.
     * @param lineInitialized - The line in which the variable lately initialized.
     */
    public void setInitialized(int lineInitialized) {
        setInitialized(lineInitialized, false);
    }
}
