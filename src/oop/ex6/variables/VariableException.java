package oop.ex6.variables;

import oop.ex6.scopes.ScopeException;

/**
 * The exception is thrown when there is a problem which is related to the variable field.
 */
public class VariableException extends ScopeException {

    private static final long serialVersionUID = 1L;

    /**
     * An empty constructor. Contains the String which will be represented when the exception is called.
     */
    public VariableException() {
        super("General variable exception");
    }

    /**
     *  A constructor of the VariableException, which gets the message which will displayed when thrown.
     * @param message - A string. Represents the message which will be displayed when the exception is
     *                thrown.
     */
    public VariableException(String message) {
        super(message);
    }
}
