package oop.ex6.variables;

import oop.ex6.scopes.NotReadableStatementException;
import oop.ex6.scopes.ScopeException;

import java.util.List;
import java.util.function.BiPredicate;

/**
 * The class handles with variable related issues. When a variable related line is read, The program passes
 * through this class, which is responsible of creating variables, variable line syntax and checking
 * assignments to variables.
 */
public class VariableHandler {

    /* A string which represents a name of a tester (non real) Variable class instance.  */
    private static final String TESTER_VARIABLE_NAME = new String("TesterVariable");

    /*
     * The method acts as a variable factory. i.e. : The method is creating TypeVariable instance, according
     * to the given parameters. The factory returns the a right instance of the  Variable class.
     * @param name - The name of the future variable.
     * @param type - The type of the future variable.
     * @param isFinal -  A flag which indicates whether the variable have been declared final.
     * @param statementLine - An int. Represents the line in which the statement have been declared.
     * @param lineIsGlobal - A flag which indicates whether the variable have been declared in a global line.
     * @return - An instance of Variable class.
     * @throws VariableException - Thrown whenever there was a problem with creating a new variable.
     */
    private static Variable factory(String name , Variable.Type type, boolean isFinal,
                                    int statementLine, boolean lineIsGlobal) throws VariableException {

        Variable initializedVariable;
        switch (type){
            case INT:
                initializedVariable = new IntVariable(type, name, lineIsGlobal, isFinal,
                        statementLine);
                break;
            case DOUBLE:
                initializedVariable = new DoubleVariable(type, name, lineIsGlobal, isFinal,
                        statementLine);
                break;
            case STRING:
                initializedVariable = new StringVariable(type, name, lineIsGlobal, isFinal,
                        statementLine);
                break;
            case BOOLEAN:
                initializedVariable = new BooleanVariable(type, name,lineIsGlobal, isFinal,
                        statementLine);
                break;
            case CHAR:
                initializedVariable = new CharVariable(type, name, lineIsGlobal, isFinal,
                        statementLine);
                break;

            default:
                initializedVariable = null;
                break;
        }
        return initializedVariable;
    }

    /*
     * The method is responsible of checking whether the variable issue line is legal in syntax wise.
     * i.e : it checks that the line has the right tokens when declaring/initializing a variable.
     * @param tokens - A list of tokens which constructs a statement in the code.
     * @param isFinal - A boolean. Tells whether the current variable is final or not.
     * @param predicate - The 'test' which the line must go throw. The test depends on the type of the
     * variable line.
     * @throws NotReadableStatementException - Thrown if the variable syntax line is not legal.
     */
    private static void checkLineSyntax(List<String> tokens, boolean isFinal,
                                           BiPredicate<List<String>, Boolean> predicate)
            throws NotReadableStatementException {
        if (!predicate.test(tokens, isFinal)) {
            throw new NotReadableStatementException();
        }
    }

    /**
     * The method manages the creation and initialization of the variable process. In addition, using the
     * checkLineSyntax method, it checks if the line is legal in syntax wise.
     * @param tokens -  A list of tokens which constructs a statement in the code.
     * @param type - The type of the future variable.
     * @param isFinal - A boolean. Tells whether the current variable is final or not.
     * @param settingVariable - A possible variable which will be set to the new created variable.
     * @param statementLine - The line in which the variable has been declared.
     * @param lineIsGlobal - A flag which indicates whether the variable is global or not.
     * @param predicate  - The 'test' which the line must go throw. The test depends on the type of the
     * variable line.
     * @return - A Variable class instance.
     * @throws ScopeException - Thrown when there was a problem with creating/initializing the variable.
     */
    public static Variable createVariableAndCheckStatement(List<String> tokens , Variable.Type type,
                                                           boolean isFinal, Variable settingVariable,
                                                           int statementLine, boolean lineIsGlobal,
                                                           BiPredicate<List<String>, Boolean> predicate)
            throws ScopeException {

        if (tokens == null || tokens.size() == 0 || type == null) {
            throw new ScopeException();
        }
        checkLineSyntax(tokens, isFinal, predicate); // throws exception
        Variable initializedVariable = factory(tokens.get(0) , type, isFinal, statementLine, lineIsGlobal);

        if (tokens.size() == 3){
            initializedVariable.setValue(tokens.get(2), settingVariable, statementLine, lineIsGlobal);
            // throws exception
        }
        return initializedVariable;
    }


    /**
     *  The method is called whenever the program encounters a call for a method or a line of a if/while
     *  statement. The method checks whether the value inside the parameter slot is legal. (checks that the
     *  value is a boolean - when if/while is called, or fits the right type parameter - when a method is
     *  called).
     * @param tokens -  A list of tokens which constructs a statement in the code.
     * @param type - The type of the future variable.
     * @param settingVariable - A possible variable which will be set to the new created variable.
     * @param statementLine -  The line in which the variable has been declared.
     * @param predicate - The 'test' which the line must go throw. The test depends on the type of the
     * variable line.
     * @return - true, if the parameter value fits the conditions.
     * @throws ScopeException - Thrown if the value does not fit the if/while/method call parameters.
     */
    public static boolean checkValueRelevance(List<String> tokens, Variable.Type type,
                                              Variable settingVariable, int statementLine,
                                              BiPredicate<List<String>, Boolean> predicate)
            throws ScopeException {

        if (tokens == null || tokens.size() == 0 || type == null) {
            throw new ScopeException();
        }

        checkLineSyntax(tokens, false, predicate); // throws exception
        Variable testerVariable = factory(TESTER_VARIABLE_NAME , type, false, statementLine, false);
        testerVariable.setValue(tokens.get(0), settingVariable, statementLine, false); // throws exception

        return true;
    }

    /**
     *  The method is called whenever there is a need to assign a value/variable to an exciting declared
     *  variable.
     * @param tokens - A list of tokens which constructs a statement in the code.
     * @param subjectVariable - The subject variable to be assigned.
     * @param settingVariable - A possible variable which will be set to the new created variable.
     * @param statementLine -  The line in which the variable has been declared.
     * @param lineIsGlobal -  A flag which indicates whether the variable is global or not.
     * @param predicate - The 'test' which the line must go throw. The test depends on the type of the
     * variable line.
     * @return - true, if the variable assignment process ended successfully.
     * @throws ScopeException - Thrown if there was a problem with assigning a value/variable.
     */
    public static boolean checkValueAssignment(List<String> tokens, Variable subjectVariable,
                                               Variable settingVariable, int statementLine,
                                               boolean lineIsGlobal,
                                               BiPredicate<List<String>, Boolean> predicate)
            throws ScopeException {
        checkLineSyntax(tokens, false, predicate); // throws exception
        subjectVariable.setValue(tokens.get(2), settingVariable, statementLine, lineIsGlobal); // throws
        // exception

        return true;
    }
}
